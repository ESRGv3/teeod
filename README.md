# Trusted Execution Environments On-Demand (TEEOD)
Trusted Execution Environments On-Demand (TEEOD) is an innovative approach to Trusted Execution Environments (TEEs) that harnesses the reconfigurable logic of FPGAs to bolster the security of critical applications. Unlike conventional TEE implementations that rely on hardware extensions or dedicated security elements, TEEOD offers a more adaptive and secure solution by dynamically generating and managing enclaves on-demand. Each enclave is customizable and includes a dedicated soft-core microcontroller (MCU), private memory, security services, and communication channels with the regular execution environment (REE).

## Structure
```
teeod
├── apps
│   └── bitcoin-wallet
├── client-lib
│   ├── Makefile
│   ├── tee_client_api.c
│   ├── tee_client_api.h
│   ├── tee_client_ipc.c
│   └── tee_client_ipc.h
├── tee-firmware
│   ├── board
│   ├── lib
│   ├── Makefile
│   ├── README.md
│   ├── ta
│   └── tee
└── vivado
    ├── ip_repo
    ├── src
    └── vivado_project.tcl
```

## Project Components
1. apps/: Placeholder for specific applications utilizing TEEOD (e.g. bitcoin wallet CA and TA)
2. client-lib: TEE client library.
3. tee-firmware: Minimal kernel that executes on enclave's soft-core MCU, Arm Cortex-m1
4. vivado: IP repository for Vivado and Vivado project script.
