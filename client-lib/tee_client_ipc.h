#ifndef TEE_CLIENT_IPC_H
#define TEE_CLIENT_IPC_H

#include "tee_client_api.h"
#include <stdint.h>

#define TEE_RST_ENCLV_ADDR  0x00A0010000
#define TEE_ITCM_ENCLV_ADDR 0x00A0000000
#define TEE_IPC_ENCLV_ADDR  0x00A0020000
#define TEE_SIGNAL_ADDR     0x00A0020000

#define TEE_OPERATION_TYPE_ADDR             (TEE_IPC_ENCLV_ADDR + 0x00)
#define TEE_OPERATION_CTX_ADDR              (TEE_IPC_ENCLV_ADDR + 0x04)

#define TEE_OPERATION_PAR_CMD_ID_ADDR       (TEE_IPC_ENCLV_ADDR + 0x14)
#define TEE_OPERATION_PAR_PARAM_TYPES_ADDR  (TEE_IPC_ENCLV_ADDR + 0x18)
#define TEE_OPERATION_PAR_PARAMS_0_a_ADDR   (TEE_IPC_ENCLV_ADDR + 0x1c)
#define TEE_OPERATION_PAR_PARAMS_0_b_ADDR   (TEE_IPC_ENCLV_ADDR + 0x20)
#define TEE_OPERATION_PAR_PARAMS_1_a_ADDR   (TEE_IPC_ENCLV_ADDR + 0x24)
#define TEE_OPERATION_PAR_PARAMS_1_b_ADDR   (TEE_IPC_ENCLV_ADDR + 0x28)
#define TEE_OPERATION_PAR_PARAMS_2_a_ADDR   (TEE_IPC_ENCLV_ADDR + 0x2c)
#define TEE_OPERATION_PAR_PARAMS_2_b_ADDR   (TEE_IPC_ENCLV_ADDR + 0x30)
#define TEE_OPERATION_PAR_PARAMS_3_a_ADDR   (TEE_IPC_ENCLV_ADDR + 0x34)
#define TEE_OPERATION_PAR_PARAMS_3_b_ADDR   (TEE_IPC_ENCLV_ADDR + 0x38)

#define TEE_OPERATION_RETURN_ADDR           (TEE_IPC_ENCLV_ADDR + 0x3C)

#define TEE_OPERATION_UUID_SIZE       4

// Value used to indicate that a new TEE_Operation has been sent
#define SIGNAL_NEW_OPERATION          1
#define SIGNAL_WAITING_FOR_OPERATION  2

#define TEE_OPERATION_OPEN_SESSION    1
#define TEE_OPERATION_INVOKE_COMMAND  2
#define TEE_OPERATION_CLOSE_SESSION   3

typedef uint32_t TEE_Result;
typedef union {
	struct {
		void *buffer;
		uint32_t size;
	} memref;
	struct {
		uint32_t a;
		uint32_t b;
	} value;
} TEE_Param;

typedef struct {
uint32_t cmd_id;
uint32_t param_types;
TEE_Param params[4];
} TEE_Operation_params;

typedef struct {
uint8_t type;
union {
uint32_t session_id;
TEEC_UUID tee_uuid;
} ctx;
TEE_Operation_params par;
TEE_Result ret;
} TEE_Operation;




int load_binary_to_enclave(void);
void TEE_Operation_send(TEE_Operation *operation);
void TEE_Operation_recv(TEE_Operation *operation);

#endif /* TEE_CLIENT_IPC_H */