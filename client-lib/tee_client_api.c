#include "tee_client_api.h"
#include "tee_client_ipc.h"
#ifdef DEBUG_PRINTF
#include <stdio.h>
#endif

#include <unistd.h>

#ifndef __aligned
#define __aligned(x) __attribute__((__aligned__(x)))
#endif
//#include <linux/tee.h>

/* How many device sequence numbers will be tried before giving up */
#define TEEC_MAX_DEV_SEQ	10

TEEC_Result TEEC_InitializeContext(const char *name, TEEC_Context *ctx)
{
  #ifdef DEBUG_PRINTF
  printf("DBG::TEEC_InitializeContext\n");
  #endif
  return TEEC_SUCCESS;
}

void TEEC_FinalizeContext(TEEC_Context *ctx)
{
  #ifdef DEBUG_PRINTF
  printf("DBG::TEEC_FinalizeContext\n");
  #endif
	if (ctx)
		close(ctx->fd);
}

TEEC_Result TEEC_OpenSession(TEEC_Context *ctx, TEEC_Session *session,
const TEEC_UUID *destination,
uint32_t connection_method, const void *connection_data,
TEEC_Operation *operation, uint32_t *ret_origin)
{
  #ifdef DEBUG_PRINTF
  printf("DBG::TEEC_OpenSession\n");
  #endif
  //FIXME: Workaround so that no need to worry about TEE loading
  load_binary_to_enclave();

  // Fill the TEE_operation struct with the necessary information
  TEE_Operation tee_operation;
  tee_operation.ctx.tee_uuid = *destination;
  tee_operation.type = TEE_OPERATION_OPEN_SESSION;
  tee_operation.par.param_types = operation->paramTypes;
  for (int i = 0; i < TEEC_CONFIG_PAYLOAD_REF_COUNT; i++) {
    tee_operation.par.params[i].value.a = operation->params[i].value.a;
    tee_operation.par.params[i].value.b = operation->params[i].value.b;
  }
  #ifdef DEBUG_PRINTF
  printf("DBG::fill the TEE_Operation\n");
  #endif
  // Send the TEE Operating and trigger the TA
  TEE_Operation_send(&tee_operation);
  //Wait for the TA to finish
  TEE_Operation_recv(&tee_operation);
  // Set the session context and ID
  session->ctx = ctx;
  session->session_id = tee_operation.ctx.session_id;
  for (int i = 0; i < TEEC_CONFIG_PAYLOAD_REF_COUNT; i++) {
    operation->params[i].value.a = tee_operation.par.params[i].value.a;
    operation->params[i].value.b = tee_operation.par.params[i].value.b;
  }
  *ret_origin = tee_operation.ret;
  return TEEC_SUCCESS;
}


void TEEC_CloseSession(TEEC_Session *session)
{
  #ifdef DEBUG_PRINTF
  printf("DBG::TEEC_CloseSession\n");
  #endif
  // Check if the input parameter is valid
  if (!session) {
    return;
  }
  TEE_Operation tee_operation;
  tee_operation.type = TEE_OPERATION_CLOSE_SESSION;
  tee_operation.ctx.session_id = session->session_id;
  // Send the TEE Operating and trigger the TA
  TEE_Operation_send(&tee_operation);

  //Wait for the TA to finish
  TEE_Operation_recv(&tee_operation);
}

TEEC_Result TEEC_InvokeCommand(TEEC_Session *session, uint32_t cmd_id,
			TEEC_Operation *operation, uint32_t *error_origin)
{
  #ifdef DEBUG_PRINTF
  printf("DBG::TEEC_InvokeCommand\n");
  #endif
  // Check if the input parameters are valid
  if (!session || !operation) {
    return TEEC_ERROR_BAD_PARAMETERS;
  }

  TEE_Operation tee_operation;
  tee_operation.type = TEE_OPERATION_INVOKE_COMMAND;
  tee_operation.ctx.session_id = session->session_id;
  tee_operation.par.param_types = operation->paramTypes;
  for (int i = 0; i < TEEC_CONFIG_PAYLOAD_REF_COUNT; i++) {
    tee_operation.par.params[i].value.a = operation->params[i].value.a;
    tee_operation.par.params[i].value.b = operation->params[i].value.b;
  }
  tee_operation.par.cmd_id = cmd_id;

  // Send the TEE Operating and trigger the TA
  TEE_Operation_send(&tee_operation);

  //Wait for the TA to finish
  TEE_Operation_recv(&tee_operation);
  for (int i = 0; i < TEEC_CONFIG_PAYLOAD_REF_COUNT; i++) {
    operation->params[i].value.a = tee_operation.par.params[i].value.a;
    operation->params[i].value.b = tee_operation.par.params[i].value.b;
  }
  *error_origin = tee_operation.ret;
  return TEEC_SUCCESS;
}
