#include "tee_client_ipc.h"
#include <string.h>
#ifdef DEBUG_PRINTF
#include <stdio.h>
#endif

//HACK: Need a way of loading binary instead of being in the same mem of the CA.
extern char cm1_bin_start;
extern char cm1_bin_end;

// Function to load a binary into the enclave
int load_binary_to_enclave(void) {
  uint32_t *itcm_addr = (uint32_t *) TEE_ITCM_ENCLV_ADDR;
  // Get the start addr and the size of the binary
  uint32_t *addr_bin = (uint32_t *) &cm1_bin_start;
  const uint32_t bin_size = &cm1_bin_end - &cm1_bin_start;
  /* Copy the binary to the ITCM memory */
  for(int i=0;i<bin_size>>2;i++) {
      *(volatile uint32_t *)(itcm_addr+i) = *(addr_bin+i);
  }

  /* Reset the enclave */
  *(volatile uint32_t *)TEE_RST_ENCLV_ADDR = (uint32_t) 0x0;
  *(volatile uint32_t *)TEE_RST_ENCLV_ADDR = (uint32_t) 0x1;
  #ifdef DEBUG_PRINTF
  printf("DBG::loading of the enclave\n");
  #endif
  return TEEC_SUCCESS;
}

void TEE_Operation_send(TEE_Operation *operation) {
  *((uint32_t *)TEE_OPERATION_TYPE_ADDR) = operation->type;
  memcpy((uint32_t *)TEE_OPERATION_CTX_ADDR, &operation->ctx.tee_uuid, TEE_OPERATION_UUID_SIZE);
  //*((uint32_t *)TEE_OPERATION_CTX_ADDR) = operation->ctx.session_id;
  *((uint32_t *)TEE_OPERATION_PAR_CMD_ID_ADDR) = operation->par.cmd_id;
  *((uint32_t *)TEE_OPERATION_PAR_PARAM_TYPES_ADDR) = operation->par.param_types;
  *((uint32_t *)TEE_OPERATION_PAR_PARAMS_0_a_ADDR) = (uint32_t)operation->par.params[0].value.a;
  *((uint32_t *)TEE_OPERATION_PAR_PARAMS_0_b_ADDR) = (uint32_t)operation->par.params[0].value.b;
  *((uint32_t *)TEE_OPERATION_PAR_PARAMS_1_a_ADDR) = (uint32_t)operation->par.params[1].value.a;
  *((uint32_t *)TEE_OPERATION_PAR_PARAMS_1_b_ADDR) = (uint32_t)operation->par.params[1].value.b;
  *((uint32_t *)TEE_OPERATION_PAR_PARAMS_2_a_ADDR) = (uint32_t)operation->par.params[2].value.a;
  *((uint32_t *)TEE_OPERATION_PAR_PARAMS_2_b_ADDR) = (uint32_t)operation->par.params[2].value.b;
  *((uint32_t *)TEE_OPERATION_PAR_PARAMS_3_a_ADDR) = (uint32_t)operation->par.params[3].value.a;
  *((uint32_t *)TEE_OPERATION_PAR_PARAMS_3_b_ADDR) = (uint32_t)operation->par.params[3].value.b;
  *((uint32_t *)TEE_OPERATION_RETURN_ADDR) = operation->ret;
 
  #ifdef DEBUG_PRINTF
  printf("DBG::operatiion sent\n");
  #endif
}

void TEE_Operation_recv(TEE_Operation *operation) {
  while(operation->type != 0) operation->type = *((uint32_t *)TEE_OPERATION_TYPE_ADDR);
  memcpy(&operation->ctx.tee_uuid,(uint32_t *)TEE_OPERATION_CTX_ADDR, TEE_OPERATION_UUID_SIZE);
  //operation->ctx.session_id = *((uint32_t *)TEE_OPERATION_CTX_ADDR);
  operation->par.cmd_id = *((uint32_t *)TEE_OPERATION_PAR_CMD_ID_ADDR);
  operation->par.param_types = *((uint32_t *)TEE_OPERATION_PAR_PARAM_TYPES_ADDR);
  operation->par.params[0].value.a = *((uint32_t *)TEE_OPERATION_PAR_PARAMS_0_a_ADDR);
  operation->par.params[0].value.b = *((uint32_t *)TEE_OPERATION_PAR_PARAMS_0_b_ADDR);
  operation->par.params[1].value.a = *((uint32_t *)TEE_OPERATION_PAR_PARAMS_1_a_ADDR);
  operation->par.params[1].value.b = *((uint32_t *)TEE_OPERATION_PAR_PARAMS_1_b_ADDR);
  operation->par.params[2].value.a = *((uint32_t *)TEE_OPERATION_PAR_PARAMS_2_a_ADDR);
  operation->par.params[2].value.b = *((uint32_t *)TEE_OPERATION_PAR_PARAMS_2_b_ADDR);
  operation->par.params[3].value.a = *((uint32_t *)TEE_OPERATION_PAR_PARAMS_3_a_ADDR);
  operation->par.params[3].value.b = *((uint32_t *)TEE_OPERATION_PAR_PARAMS_3_b_ADDR);
  operation->ret = *((uint32_t *)TEE_OPERATION_RETURN_ADDR);
  #ifdef DEBUG_PRINTF
  printf("DBG::operatiion received\n");
  #endif
}